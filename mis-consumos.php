<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Mis consumos</span>
				</h1>
				<p>
					Revisa y compara tus consumos por periodos y NICs.
				</p>
			</div>
		</div>
		<!-- Section title-->


		<!-- Date picker-->
		<div class='row date-picker-wrapper'>
			<div class='calendar-icon-wrapper valign-wrapper'>
				<i class="material-icons">date_range</i>
			</div>
			<input type="text" class="datepicker" placeholder="Selecciona un periodo">
		</div>
		<!-- Date picker-->

		<!-- Toogle buttons-->
		<div class='row'>
			<div class='col s12'>

				<div class='compare-button'>
					<div class="switch">
					  <label>
					  		<i class="material-icons">compare_arrows</i>
					  		Proyectar
					      <input type="checkbox">
					      <span class="lever"></span>
					    </label>
					  </div>
				</div>

				<div class='compare-button'>
					<div class="switch">
					  <label>
					  		<i class="material-icons">compare_arrows</i>
					  		Comparar
					      <input type="checkbox">
					      <span class="lever"></span>
					    </label>
					  </div>
				</div>

			</div>
		</div>
		<!-- Toogle buttons-->

		<div class='consumption-chart-wrapper'>
			  <div class="carousel carousel-slider center">
			  	<div class="carousel-fixed-item center see-more-nics">
			      Desliza para ver tus otros NICs <i class="material-icons">arrow_forward</i>
			    </div>
			    <a class="carousel-item" href="#one!">
			    	<div class='chart-wrapper'>
			    		<div class='row margin-fixer-bottom'>
					    	<div class='col s8 chart-title'>Consumo Global</div>
					    	<div class='col s4  chart-period'>Perido seleccionado</div>
				    	</div>
				    	<div class='chart-total'>44.2 <span>Consumo en kW (kilowatts)</span></div>
				    	<canvas id="example-chart"></canvas>
			    	</div>
			    </a>
			    <a class="carousel-item" href="#two!">
			    	<div class='chart-wrapper'>
			    		<div class='row margin-fixer-bottom'>
					    	<div class='col s8 chart-title'>NIC #32131</div>
					    	<div class='col s4  chart-period'>Perido seleccionado</div>
				    	</div>
				    	<div class='chart-total'>31.2 <span>Consumo en kW (kilowatts)</span></div>
				    	<canvas id="example-chart-2"></canvas>
			    	</div>
			    </a>
			    <a class="carousel-item" href="#three!">
			    	<div class='chart-wrapper'>
			    		<div class='row margin-fixer-bottom'>
					    	<div class='col s8 chart-title'>NIC #43223</div>
					    	<div class='col s4  chart-period'>Perido seleccionado</div>
				    	</div>
				    	<div class='chart-total'>24.2 <span>Consumo en kW (kilowatts)</span></div>
				    	<canvas id="example-chart-3"></canvas>
			    	</div>
			    </a>
			  </div>
		</div>
		
		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Detalles de consumo</span>
				</h1>
				<p>
					En esta sección podrás ver las estadisticas de consumo por NICs
				</p>
			</div>
		</div>
		<!-- Section title-->

		<div class='row consumption-table'>
			<div class='consumption-table-title'>
					Consumo por NICs
			</div>
			<div class='col s12'>
				

				<div class='consumption-table-body'>

					<div class='chart-total'>31.2 <span>Consumo en kW (kilowatts)</span></div>

					<div class="row cosumption-table-data">
						<div class='col s6'>
							NICs
						</div>
						<div class='col s6' style="text-align: right">
							Consumo durante el periodo
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #2942334
						</div>
						<div class='col s6 kw-consumption'>
							45.5 kW 
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #32131
						</div>
						<div class='col s6 kw-consumption'>
							25.5 kW 
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #54353
						</div>
						<div class='col s6 kw-consumption'>
							15.5 kW 
						</div>
					</div>

				</div>
				
			</div>
		</div>
		<!--
		
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Herramientas</span>
				</h1>
				<p>
					Proyecta y compara tus consumos, utilizando las herramientas que hemos creado para ti
				</p>
			</div>
		</div>
		

		
		<div class='row service-card'>
			<div class='col s2'>
				<i class="material-icons orange-color">info</i>
			</div>
			<div class='col s9'>
				<h2>Proyecta tu consumo</h2>
				<p>Ahorra proyectando tu consumo aquí</p>
			</div>
			<div class='next-custom valign-wrapper'>
				<a href='proyecta-tu-consumo.php'><i style="color: white;" class="material-icons">keyboard_arrow_right</i></a>
			</div>
		</div>
		
		<div class='row service-card'>
			<div class='col s2'>
				<i class="material-icons orange-color">account_circle</i>
			</div>
			<div class='col s9'>
				<h2>Compárate con otros usuarios</h2>
				<p>¿Estas gastando más de lo que debes? Entérate aquí</p>
			</div>
			<div class='next-custom valign-wrapper'>
				<i class="material-icons">keyboard_arrow_right</i>
			</div>
		</div>
		-->

	</div>



</main>

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script type="text/javascript">
	var ctx = document.getElementById('example-chart').getContext('2d');
	var chart = new Chart(ctx, {
	    // The type of chart we want to create
	    type: 'line',

	    // The data for our dataset
	    data: {
	        labels: ['Ene', 'Feb', 'Mar'],
	        datasets: [{
	            label: 'Consumo Global',
	            backgroundColor: 'rgba(255, 129, 43, 0.5)',
	            borderColor: 'rgba(255, 129, 43)',
	            data: [0, 10, 5]
	        }]
	    },

	    // Configuration options go here
	    options: {
	    	legend: {
	            display: false
	        }
	    }
	});
</script>

<script type="text/javascript">
	var ctx2 = document.getElementById('example-chart-2').getContext('2d');
	var chart2 = new Chart(ctx2, {
	    // The type of chart we want to create
	    type: 'line',

	    // The data for our dataset
	    data: {
	        labels: ['Ene', 'Feb', 'Mar'],
	        datasets: [{
	            label: 'Consumo Global',
	            backgroundColor: 'rgba(255, 129, 43, 0.5)',
	            borderColor: 'rgba(255, 129, 43)',
	            data: [0, 5, 25]
	        }]
	    },

	    // Configuration options go here
	    options: {
	    	legend: {
	            display: false
	        }
	    }
	});
</script>

<script type="text/javascript">
	var ctx3 = document.getElementById('example-chart-3').getContext('2d');
	var chart3 = new Chart(ctx3, {
	    // The type of chart we want to create
	    type: 'line',

	    // The data for our dataset
	    data: {
	        labels: ['Ene', 'Feb', 'Mar'],
	        datasets: [{
	            label: 'Consumo Global',
	            backgroundColor: 'rgba(255, 129, 43, 0.5)',
	            borderColor: 'rgba(255, 129, 43)',
	            data: [0, 25, 5]
	        }]
	    },

	    // Configuration options go here
	    options: {
	    	legend: {
	            display: false
	        }
	    }
	});
</script>

</html>