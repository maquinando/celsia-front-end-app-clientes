<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<div class='row title-inner'>
			<div class='col s2 back-button'>
				<i class="small material-icons">arrow_back</i>
			</div>
			<div class='col s8'>
				<h1>
					Energía convencional
				</h1>
			</div>
		</div>
		<!-- Section title-->

		<div class='row'>
			<div class='col s12'>
				<p class="margin-fixer-top">¡Disfrutamos hacerte la vida más fácil! Solicita la instalación de un nuevo servicio eléctrico en estos sencillos pasos.</p>
			</div>
		</div>


		<div class='row'>
			<div class='col s3 form-section-tab active-form-section-tab'><span>Paso 1</span></div>
			<div class='col s3 form-section-tab'><span>Paso 2</span></div>
			<div class='col s3 form-section-tab'><span>Paso 3</span></div>
			<div class='col s3 form-section-tab'><span>Paso 4</span></div>
		</div>

	<!-- Report Form-->
	<div class="row hiddendiv" id='first-convetional-energy'>
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12 custom-input margin-fixer-top">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="name" type="text" class="validate">
					<label for="name">Nombre</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="last_name" type="text" class="validate">
					<label for="last_name">Apellidos</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">email</i>
					<input id="email" type="text" class="validate">
					<label for="email">Correo electrónico</label>
				</div>

				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">phone</i>
					<input id="phone" type="text" class="validate">
					<label for="phone">Teléfono</label>
				</div>

				<div class="input-field col s7 custom-input">
						<i class="material-icons-outlined outlined-white prefix">picture_in_picture</i>
						<input id="id_user" type="text" class="validate">
						<label for="id_user">Cédula</label>
					</div>
					
					<div class="input-field col s4 offset-s1 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="id_province" type="text" class="validate">
						<label for="id_province">De</label>
					</div>
				
				<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="state" type="text" class="validate">
						<label for="state">Departamento</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">room</i>
					<input id="province" type="text" class="validate">
					<label for="province">Municipio</label>
				</div>
				
				<p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>He leído y acepto los Términos y condiciones</span>
			      </label>
			    </p>

				<a  href='#' class="waves-effect waves-light btn-small">Solicitar</a>


			</div>
		</form>
	</div>
	<!-- Report Form-->

	<!-- Report Form-->
	<div class="row hiddendiv" id='second-convetional-energy'>
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12 custom-input margin-fixer-top">
					<i class="material-icons-outlined outlined-white prefix">local_convenience_store</i>
					<input id="no_catrastal" type="text" class="validate">
					<label for="no_catrastal">Número catastral</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">room</i>
					<input id="home_adress" type="text" class="validate">
					<label for="home_adress">Dirección</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">room</i>
					<input id="neighbor" type="text" class="validate">
					<label for="neighbor">Barrio</label>
				</div>

				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">bar_chart</i>
					<input id="estrato" type="text" class="validate">
					<label for="estrato">Estrato</label>
				</div>

				<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="billing_adress" type="text" class="validate">
						<label for="billing_adress">Dirección de facturación</label>
				</div>
					
				<div class="row">
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Anterior</a>
					</div>
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Continuar</a>
					</div>
				</div>

			</div>
		</form>
	</div>
	<!-- Report Form-->


	<!-- Report Form-->
	<div class="row hiddendiv" id='third-convetional-energy'>
		<form class="col s12">
			<div class="row">
				<h2>Uso del servicio</h2>

				<div class="input-field col s12 custom-input margin-fixer-top borderless">
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Redicencial</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Industrial</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Oficial</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Especial</span>
					    </label>
					</p>
				</div>

				<h2>Voltaje</h2>

				<div class="row">
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">flash_on</i>
						<input id="energy" type="text" class="validate">
						<label for="energy">Voltaje</label>
					</div>
				</div>


				<h2>Modalidad de la instalación</h2>

				<div class="input-field col s12 custom-input margin-fixer-top borderless">
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Alquiler</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Venta</span>
					    </label>
					</p>
					
				</div>

				<h2>Que incluya</h2>
				
				<p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>Varilla puesta a tierra</span>
			      </label>
			    </p>

			    <p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>Acometida</span>
			      </label>
			    </p>

			    <p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>Caja</span>
			      </label>
			    </p>


			    <h2>Observaciones</h2>
			    <div class="input-field col s12 custom-input">
						  <!-- <i class="material-icons-outlined outlined-white prefix">mode_edit</i> -->
				          <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
				          <label for="icon_prefix2">Observaciones</label>
				</div>

				<h2>Empresa de aseo que atiende</h2>

				<div class="row">
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">local_shipping</i>
						<input id="trash" type="text" class="validate">
						<label for="trash">Empresa de aseo</label>
					</div>
				</div>

				<div class="row">
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Anterior</a>
					</div>
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Continuar</a>
					</div>
				</div>
				


			</div>
		</form>
	</div>
	<!-- Report Form-->


		<!-- Report Form-->
	<div class="row" id='fourth-convetional-energy'>
		<form class="col s12">
			<div class="row">
				<h2>Lee</h2>

				<div  class="col s12 upload-picture-button">
					Descarga y lee la <span>guia para la instalaciónd de nuevos servicios eléctricos.</span>
				</div>


				<h2>Descarga, diligencia y adjunta</h2>

				<div  class="col s12 upload-picture-button">
					Descargar <span>Declaración de cumplimiento del RETIE firmada por el responsable de la construcción de la instalaciones eléctricas, adjuntando el número de la matrícula profesional.</span>
				</div>

				<div  class="col s12 upload-picture-button">
						<i class="material-icons">cloud_upload</i>
						<span>Adjunta</span> la declaración
				</div>

				<div  class="col s12 upload-picture-button">
						<i class="material-icons">photo_camera</i>
						<span>Adjunta</span> fotocopia de la cédula de ciudadanía o NIT del propetario del predio.
				</div>


				<div class="row">
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Anterior</a>
					</div>
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Continuar</a>
					</div>
				</div>

			</div>
		</form>
	</div>
	<!-- Report Form-->

	

	


	</div>



</main>
<!-- main content-->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>