<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		
		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					¡Te damos la bienvenida a <br>
					<span>Celsia en línea!</span>
				</h1>
				<p class="title-text">
					Nuestros servicios y transacciones en línea están en tus manos.
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Service card-->
		<div class='row'>
			
			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnBill@2x.png'/>
				</div>
				<h2 class='service-title-home'>Paga tus facturas</h2>
				<p class='service-text'>Ingresa y búsca la factura que deseas pagar
				<!--<input placeholder="Ingrese el NIC" id="first_name" type="text" class="validate">-->
				</p>
				<a href='paga-tus-facturas.php' class="waves-effect waves-light btn-small btn-service">Pagar</a>
			</div>

			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnReportDamageIcon@2x.png'/>
				</div>
				<h2 class='service-title-home'>Reporta daños</h2>
				<p class='service-text'>¿Hay un daño? Repórtalo aquí</p>
				<a  href='reporta-un-dano.php' class="waves-effect waves-light btn-small btn-service">Reportar</a>
			</div>

			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnServices@2x.png'/>
				</div>
				<h2 class='service-title-home'>Descubre nuevos servicios</h2>
				<p class='service-text'>Nos encanta hacerte la vida más fácil ¿quieres saber cómo?</p>
				<a  href='solicitar-servicios.php' class="waves-effect waves-light btn-small btn-service">Acceder</a>
			</div>

		</div>
		<!-- Service card-->


		<!-- Service card-->
		<div class='row'>
			
			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnPayBill@2x.png'/>
				</div>
				<h2 class='service-title-home'>Consulta tus facturas</h2>
				<p class='service-text'>Consulta y paga las facturas de los NICs que tengas configurados</p>
				<a href='consulta-tus-facturas.php' class="waves-effect waves-light btn-small btn-service">Consultar</a>
			</div>

			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnTools@2x.png'/>
				</div>
				<h2 class='service-title-home'>Mis herramientas</h2>
				<p class='service-text'>Creadas especialmente para ti</p>
				<a href='mis-herramientas.php' class="waves-effect waves-light btn-small btn-service">Ver</a>
			</div>

			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnPaymentAgreetment@2x.png'/>
				</div>
				<h2 class='service-title-home'>Acuerdos de pago</h2>
				<p class='service-text'>Tenemos muchas soluciones para ti ¡conócelas!</p>
				<a href='acuerdos-de-pago.php' class="waves-effect waves-light btn-small btn-service">Solicitar</a>
			</div>
		</div>
		<!-- Service card-->



		<!-- Service card-->
		<div class='row'>
			<div class='flexbox'>
				<div class='col s4 service-card'>
					<div class='img-wrapper'>
						<img src='./img/btnSetNIC@2x.png'/>
					</div>
					<h2 class='service-title-home'>Configurar NICs</h2>
					<p class='service-text'>Agrega NICs a tu cuenta</p>
					<a class="waves-effect waves-light btn-small btn-service">Configurar</a>
				</div>
			</div>
		</div>
		<!-- Service card-->

	</div>

</main>
<!-- main content-->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>