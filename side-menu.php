<!-- top nav-->
<!--<div class="navbar-fixed">-->
	<nav>

		<div class="nav-wrapper">
			<a href="home.php" class="brand-logo hide-on-large-only">
				<img src="./img/logo@2x.png">
			</a>
			<!-- navigation menu-->
			<ul id="slide-out" class="right sidenav sidenav-fixed draggable">
				<div class='menu-title'>Nuestros servicios</div>
				<li><a href="paga-tus-facturas.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">receipt</i>Paga tus facturas</a></li>
				<li><a href="reporta-un-dano.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">notification_important</i>Reporta daños</a></li>
				<li><a href="solicitar-servicios.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">add_circle_outline</i>Nuestros servicios</a></li>
				<div class='menu-title'>Para usuarios registrados</div>
				<li><a href="consulta-tus-facturas.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">find_in_page</i>Consulta tus facturas</a></li>
				<li><a href="mis-herramientas.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">wb_sunny</i>Mis consultas</a></li>
				<li><a href="acuerdos-de-pago.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">thumb_up_alt</i>Acuerdos de pago</a></li>
				<li><a href="configura-tus-nics.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">settings</i>Configurar NICs</a></li>
				<div class='menu-title'>¿Cómo podemos ayudarte?</div>
				<li><a href="asesoria-en-linea.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">info</i>Asesoría en línea</a></li>
				<li><a href="#!"><i class="material-icons-outlined outlined-grey margin-fixer-right">room</i>Puntos de atención</a></li>
				<li><a href="preguntas-frecuentes.php"><i class="material-icons-outlined outlined-grey margin-fixer-right">help_outline</i>Preguntas frecuentes</a></li>
				<div class='menu-title'>Tenemos lo que te gusta</div>
				<li><a href="#!"><i class="material-icons-outlined outlined-grey margin-fixer-right">shopping_cart</i>Tienda CELSIA</a></li>
				<div class='menu-title'>Configuración</div>
				<!-- <li><a href="#!"><i class="material-icons-outlined outlined-grey margin-fixer-right">person</i>Perfil</a></li> -->
				<li><a href="#!"><i class="material-icons-outlined outlined-grey margin-fixer-right">power_settings_new</i>Cerrar sesión</a></li>
			</ul>
			<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			<!-- navigation menu-->
		</div>
	</nav>
<!--</div>-->
<!-- top nav-->