<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Descubre nuevos servicios</span>
				</h1>
				<p>
					Seleccionar el servicio que desees tener con nosotros.
				</p>
			</div>
		</div>
		<!-- Section title-->

    <div class='row'>
      <div class='col s12 l5 service-card-new request-service-wrapper'>

      	<div class='service-new-wrapper'>
        	<img src="./img/iconCharger@2.jpg">
    	</div>
        <h2>Cargadores para vehículos eléctricos</h2>
        <p>¿Quieres ser un pionero de la movilidad eléctrica?<br><span>¡Súmate aquí!</span></p>
        <a href="solicitar-cargador-electrico-celsia.php" class="waves-effect waves-light btn-small">Solicitar</a>
      </div>

      <div class='col s12 l5 service-card-new request-service-wrapper'>
      	<div class='service-new-wrapper'>
        	<img src="./img/iconSolarPanels.jpg">
    	</div>
        <h2>Energía solar para hogares</h2>
        <p>¿Quieres tener energía solar en tu casa?<br><span>¡Solicítala ya!</span></p>
        <a href="solicitar-energia-solar-para-hogares.php" class="waves-effect waves-light btn-small">Solicitar</a>
      </div>

      <div class='col s12 l5 service-card-new request-service-wrapper'>
      	<div class='service-new-wrapper'>
        	<img src="./img/iconWireless@2x.jpg">
    	</div>
        <h2>Internet Celsia</h2>
        <p>¿Quieres tener acceso a internet?<br><span>¡Presiona el botón!</span></p>
        <a href="solicitar-internet-celsia.php" class="waves-effect waves-light btn-small">Solicitar</a>
      </div>

      <div class='col s12 l5 service-card-new request-service-wrapper'>
      	<div class='service-new-wrapper'>
        	<img src="./img/iconElectricNet.jpg">
    	</div>
        <h2>Energía convecional</h2>
        <p>¡Disfrutamos hacerte la vida más fácil! <br><span>Solicita la instalación aquí</span></p>
        <a href="solicitar-energia-convencional-celsia.php" class="waves-effect waves-light btn-small">Solicitar</a>
      </div>

    

      
    </div>
	
	</div>

</main>
<!-- main content-->


<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>