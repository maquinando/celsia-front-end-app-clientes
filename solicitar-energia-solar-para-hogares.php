<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<div class='row title-inner'>
			<div class='col s2 back-button'>
				<i class="small material-icons">arrow_back</i>
			</div>
			<div class='col s8'>
				<h1>
					Energía sola para hogares
				</h1>
			</div>
		</div>
		<!-- Section title-->

		<div class='row'>
			<div class='col s12'>
				<p class="margin-fixer-top">Este formulario te ayudará a conocer los requisitos que debes cumplir para poder instalar los páneles solares en tu casa y disfrutar de la mejor energía.</p>
			</div>
		</div>


		<div class='row'>
			<div class='col s3 form-section-tab active-form-section-tab'><span>Paso 1</span></div>
			<div class='col s3 form-section-tab'><span>Paso 2</span></div>
		</div>

	<!-- Report Form-->
	<div class="row hiddendiv" id='first-step-charger'>
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12 custom-input margin-fixer-top">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="name" type="text" class="validate">
					<label for="name">Nombre</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="last_name" type="text" class="validate">
					<label for="last_name">Apellidos</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">email</i>
					<input id="email" type="text" class="validate">
					<label for="email">Correo electrónico</label>
				</div>
				
				<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="state" type="text" class="validate">
						<label for="state">Departamento</label>
					</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">room</i>
					<input id="province" type="text" class="validate">
					<label for="province">Municipio</label>
				</div>
				
				<p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>He leído y acepto los Términos y condiciones</span>
			      </label>
			    </p>

				<a  href='#' class="waves-effect waves-light btn-small">Solicitar</a>


			</div>
		</form>

		<div class='col s12'>
					<p>*Este producto por ahora solo está habilitado para los municipios del departamento del Valle del Cauca.</p>
		</div>
	</div>
	<!-- Report Form-->


	<!-- Report Form-->
	<div class="row " id='second-step-charger'>
		<form class="col s12">
			<div class="row">
				<h2>Lugar de la instalación</h2>

				<div class="input-field col s12 custom-input margin-fixer-top borderless">
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Casa independiente</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Casa en condominio</span>
					    </label>
					</p>
					<p>
						<label>
					      <input class="with-gap" name="group3" type="radio" checked />
					      <span>Finca</span>
					    </label>
					</p>
				</div>


				<h2>Tipo de techo</h2>

				<div class="row">
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">storefront</i>
						<input id="carbrand" type="text" class="validate">
						<label for="carbrand">Selecciona el tipo de techo</label>
					</div>
				</div>
				
				<div class="row">
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Anterior</a>
					</div>
					<div class='col s6'>
						<a  href='#' class="waves-effect waves-light btn-small">Continuar</a>
					</div>
				</div>
				


			</div>
		</form>
	</div>
	<!-- Report Form-->

	


	</div>



</main>
<!-- main content-->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>