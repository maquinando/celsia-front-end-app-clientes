<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Proyecta tu consumo</span>
				</h1>
				<p>
					Desliza el punto para seleccionar la cantidad de meses
a la que deseas proyectar tu consumo
				</p>
			</div>
		</div>
		<!-- Section title-->


		<div class='consumption-projection-wrapper'>
			
			 <form action="#">
			    <p class="range-field">
			      <input type="range" id="test5" min="1" max="12" />
			    </p>
			  </form>
			  <div class='left min-month'>1 mes</div>
			 <div class='right max-month'>12 meses</div>
		</div>

		<div class='consumption-chart-wrapper' style='min-height: 200px; height: 200px;'>
			  <div class="carousel carousel-slider center" style='min-height: 200px; height: 200px;'>
			  	<div class="carousel-fixed-item center see-more-nics">
			      Desliza para ver tus otros NICs <i class="material-icons">arrow_forward</i>
			    </div>
			    <a class="carousel-item" href="#one!" style='min-height: 200px; height: 200px;'>
			    	<div class="row">
			    		<div class='col s1'>
			    			<div class='back-custom valign-wrapper'>
								<i class="material-icons">keyboard_arrow_left</i>
							</div>
			    		</div>
			    		<div class='col s10'>
			    			<div class='chart-total'>31.2 <span>Consumo en kW (kilowatts)</span></div>
			    		</div>
			    		<div class='col s1'>
			    			<div class='next-custom valign-wrapper'>
								<i class="material-icons">keyboard_arrow_right</i>
							</div>
			    		</div>
			    	</div>
			    </a>
			    
			  </div>
		</div>
		        

		<div class='row consumption-table'>
			<div class='consumption-table-title'>
					Consumo por NICs
			</div>
			<div class='col s12'>
				

				<div class='consumption-table-body'>

					<div class='chart-total'>31.2 <span>Consumo en kW (kilowatts)</span></div>

					<div class="row cosumption-table-data">
						<div class='col s6'>
							NICs
						</div>
						<div class='col s6' style="text-align: right">
							Consumo durante el periodo
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #2942334
						</div>
						<div class='col s6 kw-consumption'>
							45.5 kW 
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #32131
						</div>
						<div class='col s6 kw-consumption'>
							25.5 kW 
						</div>
					</div>

					<div class="row">
						<div class='col s6'>
							NIC #54353
						</div>
						<div class='col s6 kw-consumption'>
							15.5 kW 
						</div>
					</div>

				</div>
				
			</div>
		</div>

		

	</div>



</main>

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


</html>