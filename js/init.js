(function($){
  $(function(){

    $('.sidenav').sidenav();
	
	$('.back-button').bind("click",function(){
		window.history.back();
	});
	


	$('.collapsible').collapsible();

	$('.modal').modal();

	 $('.datepicker').datepicker();

	 $('.carousel.carousel-slider').carousel({
	    fullWidth: true,
	     indicators: true
	  });

	 //Servicios página principal
	 $('.service-text').matchHeight(false);
	 $('.service-title-home').matchHeight(false);

	 //Herramientas
	 $('.service-card-new h1').matchHeight(false);
	 $('.service-card-new p').matchHeight(false);
	 
	 $(".new-nic-wrapper").bind("click",function(){
	 	$("#modal-configuracion-nic").modal();
		$("#modal-configuracion-nic").modal("open");
	});

	 
	 $(".dropdown-trigger").dropdown();
	 

	
  }); // end of document ready
})(jQuery); // end of jQuery name space
