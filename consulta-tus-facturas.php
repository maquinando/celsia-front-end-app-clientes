<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Consulta tus facturas</span>
				</h1>
				<p>
					Pagar tus facturas es muy fácil, solo debes tocar el NIC correspondiente y seguir los pasos
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- billing info-->
		<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              <i class="material-icons">wb_incandescent</i>NIC #2443452 
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              <span>
              	<div class="row">
            		<div class='col s8'>
            			<div class='billing-info-text'>
            				<div class='billing-main-info'>
            					Nombre del NIC
            				</div>
            				<div class='billing-secondary-info'>
            					Calle 37s #34 -125
            				</div>
            			</div>

            			<div class='billing-info-text'>
            				<div class='billing-main-info'>
            					Pago total: $250.000
            				</div>
            				<div class='billing-secondary-info'>
            					Fecha de pago: 20 de mayo de 2019
            				</div>
            			</div>
            		</div>
            		<div class='col s4 warning-text'>
            			<i class="material-icons">info</i> Vencida
            		</div>
            		<a href='#' class="waves-effect waves-light btn-small">Ir a pagar</a>
            		
            		<div class='col s12'>
            			<div class='billing-complenetary-text'>
            				<ul>
            					<li><a><i class="material-icons">remove_red_eye</i>Ver factura</a></li>
            					<li><a><i class="material-icons">email</i>Enviar al correo</a></li>
            					<li><a><i class="material-icons">thumb_up</i>Solicitar acuerdo</a></li>
            				</ul>
            			</div>
            		</div>


            	</div>
            </span>
            </div>
          </li>
    </ul>
    <!-- billing info-->

      	<!-- billing info-->
		<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              <i class="material-icons">wb_incandescent</i>NIC #2443452 
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              <span>
              	<div class="row">
            		<div class='col s8'>
            			<div class='billing-info-text'>
            				<div class='billing-main-info'>
            					Nombre del NIC
            				</div>
            				<div class='billing-secondary-info'>
            					Calle 37s #34 -125
            				</div>
            			</div>

            			<div class='billing-info-text'>
            				<div class='billing-main-info'>
            					Pago total: $250.000
            				</div>
            				<div class='billing-secondary-info'>
            					Fecha de pago: 20 de mayo de 2019
            				</div>
            			</div>
            		</div>
            		<div class='col s4 warning-text'>
            			<i class="material-icons">info</i> Vencida
            		</div>
            		<a href='#' class="waves-effect waves-light btn-small">Ir a pagar</a>
            		
            		<div class='col s12'>
            			<div class='billing-complenetary-text'>
            				<ul>
            					<li><a><i class="material-icons">remove_red_eye</i>Ver factura</a></li>
            					<li><a><i class="material-icons">email</i>Enviar al correo</a></li>
            					<li><a><i class="material-icons">thumb_up</i>Solicitar acuerdo</a></li>
            				</ul>
            			</div>
            		</div>


            	</div>
            </span>
            </div>
          </li>
      	</ul>
      	<!-- billing info-->


    <!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Pagar otra factura</span>
				</h1>
				<p>
					¿La factura que ibas a pagar no esta en la lista? Utiliza nuestro buscador para encontrarla
				</p>
			</div>
		</div>
		<!-- Section title-->
		
		<input placeholder="Ingresa un NIC" id="first_name" type="text" class="validate">
		<a href='paga-tus-facturas.php' class="waves-effect waves-light btn-small">Buscar</a>

	</div>

</main>
<!-- main content-->


<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>