<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Mis herramientas</span>
				</h1>
				<p>
					Herramientas especialmente creadas para ti
				</p>
			</div>
		</div>
		<!-- Section title-->


		<!-- Service card-->
		<div class='row '>
			<div class='flexbox'>
			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnConsumption@2x.png'/>
				</div>

				<h2 class='service-title-home'>Mis consumos</h2>
					<p class='service-text'>Controla tu consumo, ahorra y sigue creciendo</p>
					<a  href='mis-consumos.php' class="waves-effect waves-light btn-small btn-service">Ver</a>
				
			</div>

			<div class='col s4 service-card'>
				<div class='img-wrapper'>
					<img src='./img/btnSpeed@2x.png'/>
				</div>
				<h2 class='service-title-home'>Medidor de velocidad</h2>
				<p class='service-text'>Mide la velocidad de tu internet aquí</p>
				<a  href='medidor-de-velocidad.php' class="waves-effect waves-light btn-small btn-service">Medir</a>
			</div>
		</div>

		</div>
		<!-- Service card-->



	</div>

</main>
<!-- main content-->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>