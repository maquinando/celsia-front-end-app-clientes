<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Configura tus NIC</span>
				</h1>
				<p>
					Agrega NICs a tu cuenta o edita los que tienes asociados.
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Nic info-->
		<ul class="collapsible collapsible-accordion billing-info">
	          <li>
	            <div class="collapsible-header">
	              <i class="material-icons">wb_incandescent</i>NIC #2443452 
	              <div class='dropdown-arrow'>
	              	<i class="material-icons">arrow_drop_down</i>
	              </div>
	          	</div>
	            <div class="collapsible-body">
	              <span>
	              	<div class="row">
	            		<div class='col s8'>
	            			<div class='billing-info-text'>
	            				<div class='billing-main-info'>
	            					Nombre del NIC
	            				</div>
	            				<div class='billing-secondary-info'>
	            					Calle 37s #34 -125
	            				</div>
	            			</div>
	            		</div>
	            		<a href='#modal-configuracion-nic' class="waves-effect waves-light btn-small modal-trigger">Editar</a>

	            		<div class='col s12'>
	            			<div class='billing-complenetary-text'>
	            				<ul>
	            					<li><a><i class="material-icons">delete</i>Eliminar NIC</a></li>
	            				</ul>
	            			</div>
	            		</div>
	            		
	            	</div>
	            </span>
	            </div>
	          </li>
	    </ul>
	    <!-- Nic info-->

	    <!-- Nic info-->
		<ul class="collapsible collapsible-accordion billing-info">
	          <li>
	            <div class="collapsible-header">
	              <i class="material-icons">wb_incandescent</i>NIC #2443452 
	              <div class='dropdown-arrow'>
	              	<i class="material-icons">arrow_drop_down</i>
	              </div>
	          	</div>
	            <div class="collapsible-body">
	              <span>
	              	<div class="row">
	            		<div class='col s8'>
	            			<div class='billing-info-text'>
	            				<div class='billing-main-info'>
	            					Nombre del NIC
	            				</div>
	            				<div class='billing-secondary-info'>
	            					Calle 37s #34 -125
	            				</div>
	            			</div>
	            		</div>
	            		<a href='#modal-configuracion-nic' class="waves-effect waves-light btn-small modal-trigger">Editar</a>

	            		<div class='col s12'>
	            			<div class='billing-complenetary-text'>
	            				<ul>
	            					<li><a><i class="material-icons">delete</i>Eliminar NIC</a></li>
	            				</ul>
	            			</div>
	            		</div>
	            		
	            	</div>
	            </span>
	            </div>
	          </li>
	    </ul>
	    <!-- Nic info-->


	    <!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Buscar NICs</span>
				</h1>
				<p>
					Búsca el NIC que quieres agregar a tu cuenta, una vez el sistema lo haya encontrado, tocalo y así quedará vínculado a tu cuenta.
				</p>
			</div>
		</div>
		<!-- Section title-->
		
		<input placeholder="Ingresa un NIC" id="first_name" type="text" class="validate">
		<a style="margin-bottom: 40px;" href='#' class="waves-effect waves-light btn-small">Buscar</a>


				<!-- Nic info-->
		<ul class="collapsible collapsible-accordion billing-info new-nic-wrapper">
	          <li>
	            <div class="collapsible-header">
	              <i class="material-icons">wb_incandescent</i>NIC #2443452 
	              <div class='dropdown-arrow'>
	              	<i class="material-icons">add</i>
	              </div>
	          	</div>
	          </li>
	    </ul>
	    <!-- Nic info-->

	</div>

</main>
<!-- main content-->

<!-- NIC pop up -->
<div id="modal-configuracion-nic" class="modal ">
	<div class="modal-content">
		<h3>
			Agregar nuevo NIC | #2443452
		</h3>
		<p>
			Completa el formulario con los datos del NIC.
		</p>

		<!-- Report Form-->
		<div class="row margin-fixer-bottom" >
			<form class="col s12 margin-fixer-bottom">
				<div class="row">
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">post_add</i>
						<input id="last_name" type="text" class="validate">
						<label for="last_name">Últimos 4 digitos de la factura</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="nic" type="text" class="validate">
						<label for="nic">Dirección</label>
					</div>
					<div class="input-field col s12 custom-input margin-fixer-bottom">
						<i class="material-icons-outlined outlined-white prefix">person</i>
						<input id="phone" type="text" class="validate">
						<label for="phone">Nombre</label>
					</div>
				</div>
			</form>
		</div>
		<!-- Report Form-->

		<p style="padding-top: 0px; margin-top: 0px;">Selecciona un icono que se relacione con tu NIC</p>

		<div class='row'>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">home</i></div>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">store_mall_directory</i></div>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">business</i></div>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">work</i></div>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">wb_sunny</i></div>
			<div class='col s2 nic-icon-wrapper'><i class="material-icons-outlined outlined-white prefix">flash_on</i></div>
		</div>
		
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close  waves-effect waves-green btn-small">Cancelar</a>
		<a href="#!" class="modal-close waves-effect waves-green btn-small">Agregar NIC</a>
	</div>
</div>
<!-- NIC pop up -->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>