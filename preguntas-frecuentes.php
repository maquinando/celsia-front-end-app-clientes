<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Consejos y respuestas para ti</span>
				</h1>
			</div>
		</div>
		<!-- Section title-->
		
		<!-- Date picker-->
		<div class='row date-picker-wrapper'>
			<div class='calendar-icon-wrapper valign-wrapper'>
				<i class="material-icons">search</i>
			</div>
			<input placeholder="¿En qué podemos ayudarte?" id="first_name" type="text" class="validate">	
		</div>
		<!-- Date picker-->
		

		<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              Loremp ipsum cuant creadula posteros
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              	<span>
              		Lorem ipsum dolor sit amet consectetur adipiscing elit, volutpat porttitor suspendisse purus orci lacus metus imperdiet, aliquam curae suscipit pellentesque velit duis. Elementum massa lacinia sociis porta magna nam, senectus cubilia vel placerat sociosqu torquent, habitant id dictumst viverra class. Commodo netus pharetra curae laoreet iaculis vel vulputate maecenas proin id, nulla augue neque ut eu porta aenean libero hac euismod malesuada, condimentum donec class ultrices sociis lectus sed taciti montes.
            	</span>
            </div>
          </li>
      	</ul>

      	<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              Loremp ipsum cuant creadula posteros
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              	<span>
              		Lorem ipsum dolor sit amet consectetur adipiscing elit, volutpat porttitor suspendisse purus orci lacus metus imperdiet, aliquam curae suscipit pellentesque velit duis. Elementum massa lacinia sociis porta magna nam, senectus cubilia vel placerat sociosqu torquent, habitant id dictumst viverra class. Commodo netus pharetra curae laoreet iaculis vel vulputate maecenas proin id, nulla augue neque ut eu porta aenean libero hac euismod malesuada, condimentum donec class ultrices sociis lectus sed taciti montes.
            	</span>
            </div>
          </li>
      	</ul>

      			<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              Loremp ipsum cuant creadula posteros
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              	<span>
              		Lorem ipsum dolor sit amet consectetur adipiscing elit, volutpat porttitor suspendisse purus orci lacus metus imperdiet, aliquam curae suscipit pellentesque velit duis. Elementum massa lacinia sociis porta magna nam, senectus cubilia vel placerat sociosqu torquent, habitant id dictumst viverra class. Commodo netus pharetra curae laoreet iaculis vel vulputate maecenas proin id, nulla augue neque ut eu porta aenean libero hac euismod malesuada, condimentum donec class ultrices sociis lectus sed taciti montes.
            	</span>
            </div>
          </li>
      	</ul>

	</div>



	

</main>

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>