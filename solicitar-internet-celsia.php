<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<div class='row title-inner'>
			<div class='col s2 back-button'>
				<i class="small material-icons">arrow_back</i>
			</div>
			<div class='col s8'>
				<h1>
					Internet Celsia
				</h1>
			</div>
		</div>
		<!-- Section title-->

		<div class='row'>
			<div class='col s12'>
				<p class="margin-fixer-top">Llena el formulario para solicitar la instalación de internet en tu casa.</p>
			</div>
		</div>


	<!-- Report Form-->
	<div class="row" id='first-step-charger'>
		<form class="col s12">
			<div class="row">
				<div class="input-field col s12 custom-input margin-fixer-top">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="name" type="text" class="validate">
					<label for="name">Nombre</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">person</i>
					<input id="last_name" type="text" class="validate">
					<label for="last_name">Apellidos</label>
				</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">email</i>
					<input id="email" type="text" class="validate">
					<label for="email">Correo electrónico</label>
				</div>

				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">phone</i>
					<input id="phone" type="text" class="validate">
					<label for="phone">Teléfono</label>
				</div>
				
				<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="state" type="text" class="validate">
						<label for="state">Departamento</label>
					</div>
				<div class="input-field col s12 custom-input">
					<i class="material-icons-outlined outlined-white prefix">room</i>
					<input id="province" type="text" class="validate">
					<label for="province">Municipio</label>
				</div>

				<div class="input-field col s8 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="address" type="text" class="validate">
						<label for="address">Dirección</label>
					</div>
					<div class="input-field col s4" style="padding-right: 0px">
						<a  href='#modal-maps' class="waves-effect waves-light btn-small modal-trigger map-button">Mapa</a>
					</div>

					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">wifi</i>
						<input id="plan" type="text" class="validate">
						<label for="plan">Plan de internet</label>
					</div>
				
				<p class='check-wrapper'>
			      <label>
			        <input type="checkbox" />
			        <span>He leído y acepto los Términos y condiciones</span>
			      </label>
			    </p>

				<a  href='#' class="waves-effect waves-light btn-small">Solicitar</a>


			</div>
		</form>

		<div class='col s12'>
					<p>*Este producto por ahora solo está habilitado para los municipios del departamento del Valle del Cauca.</p>
		</div>
	</div>
	<!-- Report Form-->


	

	</div>



</main>
<!-- main content-->


<div id="modal-maps" class="modal">
	<div class="modal-content">
		<h3>
			Selecciona la ubicación de tu casa
		</h3>
		<p>
			Una vez la hayas encontrado toca el botón aceptar.
		</p>
		<div class="mapouter">
			<div class="gmap_canvas">
				<iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=Cali&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close  waves-effect waves-green btn-small">Cancelar</a>
		<a href="#!" class="modal-close waves-effect waves-green btn-small">Aceptar</a>
	</div>
</div>
<!-- Google Maps -->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>