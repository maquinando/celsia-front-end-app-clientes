<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container banner-fixer'>


		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Paga tus facturas</span>
				</h1>
				<p>
					Pagar tus facturas es muy fácil, solo debes ingresar el NIC correspondiente y presionar el botón buscar. 
					<br>
					<br>
					Una vez el sistema haya encontrado tus facturas, toca el nic y sigue los pasos. 
				</p>
			</div>
		</div>
		<!-- Section title-->

		

		<input placeholder="Ingresa un NIC" id="first_name" type="text" class="validate">
		<a href='paga-tus-facturas.php' class="waves-effect waves-light btn-small">Buscar</a>


		<!-- Section title-->
		<div class='row title-inner'>
			<!--<div class='col s2 back-button'>
				<i class="small material-icons">arrow_back</i>
			</div>-->
			<div class='col s12' style="padding-top: 30px;">
				<h1>
					Facturas encontradas
				</h1>
				<h3>Paga tus facturas</h3>
			</div>
		</div>
		<!-- Section title-->

		<ul class="collapsible collapsible-accordion billing-info">
          <li>
            <div class="collapsible-header">
              <i class="material-icons">wb_incandescent</i>NIC #2443452 
              <div class='dropdown-arrow'>
              	<i class="material-icons">arrow_drop_down</i>
              </div>
          	</div>
            <div class="collapsible-body">
              <span>
              	<div class="row">
            		<div class='col s8'>
            			<div class='billing-info-text'>
            				<div class='billing-main-info'>
            					Pago total: $250.000
            				</div>
            				<div class='billing-secondary-info'>
            					Fecha de pago: 20 de mayo de 2019
            				</div>
            			</div>
            		</div>
            		<div class='col s4 warning-text'>
            			<i class="material-icons">info</i> Vencida
            		</div>
            		<a href='#' class="waves-effect waves-light btn-small">Ir a pagar</a>
            	</div>
            </span>
            </div>
          </li>
      	</ul>

	</div>

	<div class="banner-wrapper-main hide-on-med-and-down">
		  <div class="carousel carousel-slider center banner-ads">
		    
		    <div class="carousel-item red white-text" href="#one!" id='slide-3-main'>
		      <h2>Paga tus facturas<br><span> desde cualquier lugar</span></h2>
		      <p class="white-text">En nuestro portal puedes pagar y consultar tus facturas desde la comodidad de tu casa.</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Más información</a>
		    </div>
		    
		    <!--
		    <div class="carousel-item amber white-text" href="#two!">
		      <h2>Second Panel</h2>
		      <p class="white-text">This is your second panel</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelo ahora</a>
		    </div>
			-->

		  </div>
	</div>

</main>
<!-- main content-->


<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>