<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					<span>Reporta daños</span>
				</h1>
				<p>
					Completa este formulario para reportar un daño. Nuestro equipo lo reparará lo más pronto posible.
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Report Form-->
		<div class="row">
			<form class="col s12">
				<div class="row">
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">person</i>
						<input id="name" type="text" class="validate">
						<label for="name">Nombre</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">person</i>
						<input id="last_name" type="text" class="validate">
						<label for="last_name">Apellidos</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">wb_incandescent</i>
						<input id="nic" type="text" class="validate">
						<label for="nic">NIC</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">phone</i>
						<input id="phone" type="text" class="validate">
						<label for="phone">Teléfono</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">email</i>
						<input id="email" type="text" class="validate">
						<label for="email">Correo electrónico</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="state" type="text" class="validate">
						<label for="state">Departamento</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="province" type="text" class="validate">
						<label for="province">Municipio</label>
					</div>
					<div class="input-field col s8 custom-input">
						<i class="material-icons-outlined outlined-white prefix">room</i>
						<input id="address" type="text" class="validate">
						<label for="address">Dirección del daño</label>
					</div>
					<div class="input-field col s4" style="padding-right: 0px">
						<a  href='#modal-maps' class="waves-effect waves-light btn-small modal-trigger map-button">Mapa</a>
					</div>
					<div class="input-field col s12 custom-input">
						  <!-- <i class="material-icons-outlined outlined-white prefix">mode_edit</i> -->
				          <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
				          <label for="icon_prefix2">Descripción</label>
					</div>

					<div  class="col s12 upload-picture-button">
						<i class="material-icons">photo_camera</i>
						<span>Adjunta fotos</span> que evidencien el daño
					</div>

					<a  href='reporta-un-dano.php' class="waves-effect waves-light btn-small">Reportar</a>
				</div>
			</form>
		</div>
		<!-- Report Form-->


	</div>

</main>
<!-- main content-->

<!-- Google Maps -->
<div id="modal-maps" class="modal">
	<div class="modal-content">
		<h3>
			Selecciona la ubicación del daño
		</h3>
		<p>
			Una vez la hayas encontrado toca el botón aceptar.
		</p>
		<div class="mapouter">
			<div class="gmap_canvas">
				<iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=Cali&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close  waves-effect waves-green btn-small">Cancelar</a>
		<a href="#!" class="modal-close waves-effect waves-green btn-small">Aceptar</a>
	</div>
</div>
<!-- Google Maps -->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>