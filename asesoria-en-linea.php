<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container banner-fixer'>

		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					Conéctate con<br>
					<span>nuestro equipo</span>
				</h1>
				<p>
					Elige una opción para comunicarte con nosotros.
				</p>
			</div>
		</div>
		<!-- Section title-->
		<a  href='#' class="waves-effect waves-light btn-small customer-service-button">Chat en línea (LuzI.A.)</a>
		<a  href='#' class="waves-effect waves-light btn-small customer-service-button">Cuéntanos tu experiencia</a>
		<a  href='#' class="waves-effect waves-light btn-small customer-service-button">Video llamada</a>
		<a  href='#' class="waves-effect waves-light btn-small customer-service-button">Llámanos</a>

		<div class='secondary-link-wrapper'>
			<a href="#" ><span>Al presionar alguna de las opciones aceptas los</span>  Términos & Condiciones</a>
		</div>
	</div>

		<div class="banner-wrapper-main hide-on-med-and-down">
		  <div class="carousel carousel-slider center banner-ads">
		    
		    <div class="carousel-item red white-text" href="#one!" id='slide-2-main'>
		      <h2>Estamos para ti<br><span> 24/7</span></h2>
		      <p class="white-text">Creamos múltiples formas de atención para que nunca te sientas solo.</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelas aquí</a>
		    </div>
		    
		    <!--
		    <div class="carousel-item amber white-text" href="#two!">
		      <h2>Second Panel</h2>
		      <p class="white-text">This is your second panel</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelo ahora</a>
		    </div>
			-->

		  </div>
	</div>

</main>

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>