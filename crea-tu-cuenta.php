<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main margin-fixer-bottom'>
			<div class='col s12'>
				<h1>
					<span>Crea tu cuenta</span>
				</h1>
				<p>
					Crea tu cuenta para acceder a todos nuestros servicios y 
transacciones en línea desde un solo lugar
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Report Form-->
		<div class="row">
			<form class="col s12">
				<div class="row">
					<div class="input-field col s12 custom-input margin-fixer-top">
						<i class="material-icons-outlined outlined-white prefix">person</i>
						<input id="name" type="text" class="validate">
						<label for="name">Nombre</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">person</i>
						<input id="last_name" type="text" class="validate">
						<label for="last_name">Apellidos</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">email</i>
						<input id="email" type="text" class="validate">
						<label for="email">Correo electrónico</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">lock</i>
						<input id="nic" type="text" class="validate">
						<label for="nic">Contraseña</label>
					</div>
					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">phone</i>
						<input id="phone" type="text" class="validate">
						<label for="phone">Teléfono</label>
					</div>
					
					<p class='check-wrapper'>
				      <label>
				        <input type="checkbox" />
				        <span>He leído y acepto los Términos y condiciones</span>
				      </label>
				    </p>

					<a  href='home.php' class="waves-effect waves-light btn-small">Crear cuenta</a>


				</div>
			</form>
			<div class='col s12' id='login-in-link-wrapper'>
						<a href="index.php" ><span>¿Ya tienes una cuenta?</span> Ingresa aquí</a>
			</div>
		</div>
		<!-- Report Form-->


	</div>

</main>
<!-- main content-->


            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>