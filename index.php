<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container small-container banner-fixer' >

		<!-- Section title-->
		<div class='row title-main margin-fixer-bottom'>
			<div class='col s12'>
				<h1>
					<span>Inicia sesión</span>
				</h1>
				<p>Inicia sesión para acceder a todos los servicios que hemos creado especialmente para ti.</p>
			</div>
		</div>

		
		<!-- Section title-->

		<!-- Report Form-->
		<div class="row">
			<form class="col s12">
				<div class="row">
					
					<div class="input-field col s12 custom-input margin-fixer-top">
						<i class="material-icons-outlined outlined-white prefix">email</i>
						<input id="email" type="text" class="validate">
						<label for="email">Ingresa tu correo electrónico</label>
					</div>

					<div class="input-field col s12 custom-input">
						<i class="material-icons-outlined outlined-white prefix">lock</i>
						<input id="password" type="text" class="validate">
						<label for="password">Ingresa tu contraseña</label>
					</div>

					<p class='check-wrapper'>
				      <label>
				        <input type="checkbox" />
				        <span>No cerrar sesión</span>
				      </label>
				    </p>
					
					<a  href='home.php' class="waves-effect waves-light btn-small">Iniciar sesión</a>
				</div>
			</form>
			<div class='col s12' id='password-recovery-link-wrapper'>
				<a href="recupera-tu-contrasena.php" >¿Olvidaste tu contraseña?</a>
			</div>
			<div class='col s12'>
				<div class='separator'>
					También puedes ingresar con
				</div>
			</div>

			<div class='col s12 btn-login-wrapper-social'>
				<a  href='#' class="btn-small fb-button">Facebook</a>
			</div>

			<div class='col s12  btn-login-wrapper-social'>
				<a  href='#' class="waves-effect waves-light btn-small g-button">Google</a>
			</div>

			<div class='col s12' id='sign-up-link-wrapper'>
				<a href="crea-tu-cuenta.php" ><span>¿Aún no estas registrado?</span> Crea tu cuenta</a>
			</div>

		</div>
		<!-- Report Form-->

	</div>
	
	<div class="banner-wrapper-main hide-on-med-and-down">
		  <div class="carousel carousel-slider center banner-ads">
		    
		    <div class="carousel-item red white-text" href="#one!" id='slide-1-main'>
		      <h2>La nueva era de<br><span> la energía</span></h2>
		      <p class="white-text">Es brindarte soluciones eficiente con nuestro modelo de negocio.</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelo ahora</a>
		    </div>
		    
		    <!--
		    <div class="carousel-item amber white-text" href="#two!">
		      <h2>Second Panel</h2>
		      <p class="white-text">This is your second panel</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelo ahora</a>
		    </div>

		    <div class="carousel-item amber white-text" href="#two!">
		      <h2>Third Panel</h2>
		      <p class="white-text">This is your second panel</p>
		      <a class="btn waves-effect" href="https://google.com" target="_blank">Conócelo ahora</a>
		    </div>-->
			

		  </div>
	</div>

	
</main>


            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>