<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		
		<!-- Section title-->
		<div class='row title-main'>
			<div class='col s12'>
				<h1>
					¡Te damos la bienvenida a <br>
					<span>Celsia en línea!</span>
				</h1>
				<p>
					Nuestros servicios y transacciones en línea están en tus manos. Inicia sesión o crea tu cuenta y empieza a disfrutarlos. 
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnBill@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Paga tus facturas</h2>
				<p>Ingresa el NIC de la factura que deseas pagar</p>
				<input placeholder="Ingrese el NIC" id="first_name" type="text" class="validate">
				<a href='paga-tus-facturas.php' class="waves-effect waves-light btn-small">Ir a pagar</a>
			</div>
		</div>
		<!-- Service card-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnReportDamageIcon@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Reporta daños</h2>
				<p>¿Hay un daño? Repórtalo aquí</p>
				<a  href='reporta-un-dano.php' class="waves-effect waves-light btn-small">Reportar</a>
			</div>
		</div>
		<!-- Service card-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnServices@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Descubre nuevos servicios</h2>
				<p>Nos encanta hacerte la vida más fácil ¿quieres saber cómo?</p>
				<a  href='solicitar-servicios.php' class="waves-effect waves-light btn-small">Accede a nuevos servicios</a>
			</div>
		</div>
		<!-- Service card-->


		<!-- Section title-->
		<div class='row'>
			<div class='col s12'>
				<h1>
					Servicios exclusivos <br>
					<span>para usuarios registrados</span>
				</h1>
				<p>
					Regístrate para acceder a todos los servicios que hemos creado especialmente para ti.
				</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnPayBill@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Consulta tus facturas</h2>
				<p>Consulta y paga las facturas de los NICs que tengas configurados</p>
				<a href='consulta-tus-facturas.php' class="waves-effect waves-light btn-small">Consultar mis facturas</a>
			</div>
		</div>
		<!-- Service card-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnTools@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Mis herramientas</h2>
				<p>Creadas especialmente para ti</p>
				<a href='mis-herramientas.php' class="waves-effect waves-light btn-small">Ver mis herramientas</a>
			</div>
		</div>
		<!-- Service card-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnPaymentAgreetment@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Acuerdos de pago</h2>
				<p>Tenemos muchas soluciones para ti ¡conócelas!</p>
				<a href='acuerdos-de-pago.php' class="waves-effect waves-light btn-small">Solicitar acuerdo</a>
			</div>
		</div>
		<!-- Service card-->

		<!-- Service card-->
		<div class='row service-card'>
			<div class='col s2 img-wrapper'>
				<img src='./img/btnSetNIC@2x.png'/>
			</div>
			<div class='col s10'>
				<h2>Configurar NICs</h2>
				<p>Agrega NICs a tu cuenta</p>
				<a class="waves-effect waves-light btn-small">Configurar</a>
			</div>
		</div>
		<!-- Service card-->

	</div>

</main>
<!-- main content-->

<?php
	/*Inserts the footer into the project*/
	include("footer.php");
?>
            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>