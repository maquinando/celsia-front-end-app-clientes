<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8" />

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
	Remove this if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CELSIA</title>
	<meta name="description" content="" />
	<meta name="author" content="" />

	<meta name="viewport" content="width=device-width; initial-scale=1.0" />

	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<!-- <link rel="shortcut icon" href="/favicon.ico" /> -->
	<!-- <link rel="apple-touch-icon" href="/apple-touch-icon.png" /> -->
	
	<?php
	/*Inserts common style into the project*/
		include("common-styles.php");
	?>	

	
	
</head>

<body>


<?php
	/*Inserts the header into the project*/
	include("header.php");
?>

<!-- main content-->
<main>
	
	<div class='container'>

		<!-- Section title-->
		<div class='row title-main '>
			<div class='col s12'>
				<h1>
					<span>Recupera tu contraseña</span>
				</h1>
				<p>Ingresa tu correo electrónico y nosotros te enviaremos un vínculo para que puedas crear una nueva contraseña.</p>
			</div>
		</div>
		<!-- Section title-->

		<!-- Report Form-->
		<div class="row">
			<form class="col s12">
				<div class="row">
					
					<div class="input-field col s12 custom-input margin-fixer-top">
						<i class="material-icons-outlined outlined-white prefix">email</i>
						<input id="email" type="text" class="validate">
						<label for="email">Correo electrónico</label>
					</div>
					
					<a  href='home.php' class="waves-effect waves-light btn-small">Recuperar contraseña</a>
				</div>
			</form>
			<div class='col s12' id='login-in-link-wrapper'>
						<a href="index.php" ><span>¿Recordaste tu contraseña?</span> Ingresa aquí</a>
			</div>
		</div>
		<!-- Report Form-->

		

	</div>

	
</main>


            

</body>

<?php
	/*Inserts common scripts into the project*/
	include("common-scripts.php");
?>	

</html>